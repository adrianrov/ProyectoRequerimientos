CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `vista_reservasxpersona` AS
    select 
        `t1`.`idReservacion` AS `ID_RESERVACION`,
        `t2`.`Nombre` AS `Nombre`,
        `t1`.`Fecha` AS `Fecha de Reserva`,
        `t3`.`Numero` AS `#_HAB`
    from
        (((`reservaciones` `t1`
        join `habitaciones` `t3`)
        join `habitacionesdereservacion` `t4`)
        join `tarjetas` `t2`)
    where
        ((`t1`.`idReservacion` = `t4`.`idReservacion`)
            and (`t1`.`idUsuario` = `t2`.`idUsuario`)
            and (`t3`.`idHabitacion` = `t4`.`idHabitacion`))