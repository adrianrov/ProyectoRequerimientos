﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        //private hrdbEntities db = new hrdbEntities();
        public ActionResult Index()
        {
            hrdbEntities db = new hrdbEntities();
            List<noticias> noticias = db.noticias
               .OrderByDescending(x => x.idNoticia)
               .ToList<noticias>();
            return View(noticias);
            /*ViewBag.Noticias = ListaNoticias();
            //ViewBag.NewsList = ;

            //return View(db.noticias.ToList());
            return View();*/
        }

        [Authorize]
        public ActionResult Reservation()
        {
            ViewBag.Message = "Your app reservation page.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public static IList<noticias> ListaNoticias()
        {
            using (hrdbEntities db = new hrdbEntities())
            {
                return noticias.Select(db).ToList<noticias>();
            }
        }
    }
}
