﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class NewsModels
    {
        public int idNoticias;

        [Required(ErrorMessage = "¡Por favor indique el titulo de la noticia!")]
        [StringLength(100, ErrorMessage = "¡El titulo de la notica es muy grande!")]
        [Display(Name = "Titulo")]
        public string Titulo;

        [Required(ErrorMessage = "¡Por favor indique el contenido de la noticia!")]
        [StringLength(10000, ErrorMessage = "¡El contenido de la notica es muy grande!")]
        [Display(Name = "Contenido de Noticia")]
        public string Contenido;

        public DateTime Fecha;
    }
}
